Release Notes
-------------
1.2.0
-----
* Added the ability to use SlackWebhook handler.

1.1.1
-----
* Now logs down to DEBUG level.

1.0.1
-----
* Fixed issue with incorrectly returned "this" instead of "$this".

1.0.0
-----
* Initial Release.